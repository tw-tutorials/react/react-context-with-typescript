import React, { createContext } from 'react';

// A common use case for using context is to provide theme information to components in an app. We are going to provide a color value in a context that components can use.

// 7. Explicitly setting the context type
// We are going to enhance the context from the last post so that the theme can be updated by consumers.
// In the last post, the type for the context was inferred from the default value, which was a simple string.The type for our enhanced context is going to be a little more complex
type ThemeContextType = {
	// So, there will be a theme property containing the current value for the theme and a setTheme method to update the current theme
	theme: string;
	setTheme: (value: string) => void;
};

// 1. Let’s start by creating our theme using Reacts createContext function
// 2. We are required to provide a default value for the context, which in our case is "white".
const defaultTheme = 'white';
// const ThemeContext = createContext(defaultTheme);

// 8. Reacts createContext function expects us to supply an argument for initial context value. We can supply a default value for the theme property, but it doesn’t make sense to provide a default implementation for the setTheme method. So, a simple approach is to pass in undefined as the initial value
// The type of the context value is inferred to be undefined if in strict mode or any if not
// So, ThemeContext isn’t typed as we require at the moment. How can we explicitly specify the type for the context when using createContext? Well, createContext is a generic function. So, we can pass in the type for the context value as a generic parameter
// Therefore, we can type our context is as follows
export const ThemeContext = createContext<ThemeContextType>(undefined!);

type Props = {
	children: React.ReactNode;
};

// 3. Creating a Context provider
export const ThemeProvider = ({ children }: Props) => {
	// 3.1. We hold the theme value in the state. This means that when it changes, React will automatically re-render the provider’s children with the new theme.
	const [theme, setTheme] = React.useState(defaultTheme);

	// 3.2. We get the current theme value using Reacts useEffect hook and update the theme state value.
	React.useEffect(() => {
		// We would get the theme from a web API / local storage in a real app
		// We have hardcoded the theme in our example
		const currentTheme = 'lightblue';
		setTheme(currentTheme);
	}, []);
	const [useTheme, CtxProvider] = createCtx<ThemeContextType>();

	// 3.4. Out theme provider component returns the Provider component within the context with our theme value. The provider is wrapped around all the children in the component tree.
	return (
		// 9. Using the enhanced context in the provider
		// The modification to the context provider, from the last post, is to the value we provide from it. Instead of a simple string, it is now an object containing the theme property and the setTheme method
		// <ThemeContext.Provider value={theme}>
		// <ThemeContext.Provider value={{ theme, setTheme }}>
		// 	{/*  */}
		// 	{children}
		// </ThemeContext.Provider>
		<CtxProvider value={{ theme, setTheme }}>
			{/*  */}
			{children}
		</CtxProvider>
	);
};

// 4. Creating a custom hook for consuming the context
// We can create a custom hook that will allow function components to consume our context
export const useTheme = () => React.useContext(ThemeContext);

function createCtx<ContextType>() {
	const ctx = createContext<ContextType | undefined>(undefined);
	function useCtx() {
		const c = React.useContext(ctx);
		if (!c) {
			throw new Error('useCtx must be inside a Provider with a value');
		}
		return c;
	}
	return [useCtx, ctx.Provider] as const;
}
