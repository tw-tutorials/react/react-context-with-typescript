import React from 'react';
import { Header } from './components/Header';
import { ThemeProvider } from './contexts/theme';

export const App: React.FC = () => {
	return (
		// 5. Adding the provider to the component tree
		<ThemeProvider>
			<Header />
		</ThemeProvider>
	);
};
