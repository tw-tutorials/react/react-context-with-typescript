import React, { CSSProperties } from 'react';
import { ThemeContext, useTheme } from '../contexts/theme';

const Header: React.FC = () => {
	const { theme, setTheme } = useTheme();

	return (
		<div style={{ backgroundColor: value!.theme as any }}>
			<select value={value!.theme} onChange={(e) => value!.setTheme(e.currentTarget.value)}>
				<option value="white">White</option>
				<option value="lightblue">Blue</option>
				<option value="lightgreen">Green</option>
			</select>
			<span>Hello!</span>
		</div>
	);
};
