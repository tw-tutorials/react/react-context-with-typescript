import React, { createContext, useContext, useEffect, useState } from 'react';
import { createCtx } from '.';

type UserType = {
	username: string;
	password?: string;
};

type AuthContextType = {
	loading: boolean;
	error: any;
	authenticated: boolean;
	user: UserType;
	signInWithEmailAndPassword: (email: string, password: string) => Promise<void>;
	signOut: () => Promise<void>;
};

// export const AuthContext = createContext<AuthContextType>(undefined!);
// export const useAuth = () => useContext(AuthContext);

export const [useAuth, CtxProvider] = createCtx<AuthContextType>();

export const AuthProvider: React.FC = (props) => {
	const [loading, setLoading] = useState<boolean>(false);
	const [error, setError] = useState<any>({});
	const [authenticated, setAuthenticated] = useState<boolean>(false);
	const [user, setUser] = useState<UserType>(undefined!);

	const signInWithEmailAndPassword = async (email: string, password: string) => {
		setLoading(true);
		try {
			console.log('Signing in');
			await _signInWithEmailAndPassword(email, password);
			setAuthenticated(true);
			setUser({ username: email });
			console.log('signed in');
		} catch (err) {
			setError(err);
		} finally {
			setLoading(false);
		}
	};

	const signOut = async () => {
		setLoading(true);
		try {
			setAuthenticated(false);
			setUser(undefined!);
		} catch (err) {
			setError(err);
		} finally {
			setLoading(false);
		}
	};

	// const [useAuth, CtxProvider] = createCtx<AuthContextType>();

	return (
		<CtxProvider
			value={{ loading, error, authenticated, user, signInWithEmailAndPassword, signOut }}
		>
			{/*  */}
			{props.children}
		</CtxProvider>
	);
};

const _signInWithEmailAndPassword = async (email: string, password: string): Promise<void> => {
	return new Promise((resolve) =>
		setTimeout(() => {
			resolve();
		}, 2000)
	);
};
