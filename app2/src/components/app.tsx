import React from 'react';
import { AuthProvider, useAuth } from '../contexts/auth';
import { Navbar } from './Navbar';
import { Login } from './Login';

export const App: React.FC = () => {
	const { authenticated } = useAuth();
	return (
		<React.Fragment>
			<Navbar />
			{!authenticated && <Login />}
		</React.Fragment>
	);
};
