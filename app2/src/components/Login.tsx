import React, { useContext, useState } from 'react';
import { useAuth } from '../contexts/auth';

export const Login: React.FC = () => {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const { loading, signInWithEmailAndPassword } = useAuth();

	const submit = (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();

		signInWithEmailAndPassword(email, password);
	};

	return (
		<form onSubmit={submit} style={{ backgroundColor: loading ? 'red' : 'none' }}>
			<input
				type="text"
				placeholder="Email"
				value={email}
				onChange={(e) => setEmail(e.currentTarget.value)}
			/>
			<input
				type="password"
				placeholder="Password"
				value={password}
				onChange={(e) => setPassword(e.currentTarget.value)}
			/>
			<button type="submit" disabled={loading}>
				Login
			</button>
		</form>
	);
};
