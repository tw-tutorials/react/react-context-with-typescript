import React from 'react';
import { useAuth } from '../contexts/auth';

export const Navbar: React.FC = () => {
	const { authenticated, user, signOut } = useAuth();

	return (
		<nav>
			{authenticated && (
				<div>
					<span>
						Logged in as <i>{user?.username}</i>
					</span>
					<button onClick={signOut}>Logout</button>
				</div>
			)}

			{!authenticated && <span>Not logged in</span>}
		</nav>
	);
};
